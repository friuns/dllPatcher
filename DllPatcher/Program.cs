﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mono.Cecil;
using Mono.Cecil.Rocks;

namespace DllPatcher
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (var path in args)
            {
                if (!File.Exists(path + "old"))
                    File.Move(path, path + "old");
                AssemblyDefinition a = AssemblyDefinition.ReadAssembly(path + "old");
                foreach (var b in a.MainModule.GetTypes())
                {
                    if (!b.IsNestedPrivate && b.IsNotPublic)
                    {
                        b.IsPublic = true;
                        b.IsNotPublic = false;
                    }
                    foreach (FieldDefinition c in b.Fields)
                        if (!c.IsPublic && !c.IsPrivate)
                            c.IsPublic = true;

                    foreach (var c in b.Methods)
                    {
                        if (!c.IsPublic && !c.IsPrivate)
                            c.IsPublic = true;
                    }
                }
                File.Delete(path);
                a.MainModule.Write(path);
            }
        }
    }
}
